package data_types;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by omar on 12/10/16.
 * Hello
 */
public class PackagesClips implements Parcelable {
    public int id;
    public int packageId;
    public int clipId;
    public int number;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeInt(this.packageId);
        dest.writeInt(this.clipId);
        dest.writeInt(this.number);
    }

    public PackagesClips() {
    }

    protected PackagesClips(Parcel in) {
        this.id = in.readInt();
        this.packageId = in.readInt();
        this.clipId = in.readInt();
        this.number = in.readInt();
    }

    public static final Parcelable.Creator<PackagesClips> CREATOR = new Parcelable.Creator<PackagesClips>() {
        @Override
        public PackagesClips createFromParcel(Parcel source) {
            return new PackagesClips(source);
        }

        @Override
        public PackagesClips[] newArray(int size) {
            return new PackagesClips[size];
        }
    };
}
