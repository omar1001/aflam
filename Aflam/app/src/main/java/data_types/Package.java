package data_types;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by omar on 12/10/16.
 * Hello
 */
public class Package implements Parcelable {
    public int id;
    public String color;
    public int points;
    public int clipsNumber;
    public List<Clip> clips;
    public int solvedClipsCount = 0;
    public int number;

    public Package() {

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.color);
        dest.writeInt(this.points);
        dest.writeInt(this.clipsNumber);
        dest.writeTypedList(this.clips);
        dest.writeInt(this.solvedClipsCount);
        dest.writeInt(this.number);
    }

    protected Package(Parcel in) {
        this.id = in.readInt();
        this.color = in.readString();
        this.points = in.readInt();
        this.clipsNumber = in.readInt();
        this.clips = in.createTypedArrayList(Clip.CREATOR);
        this.solvedClipsCount = in.readInt();
        this.number = in.readInt();
    }

    public static final Creator<Package> CREATOR = new Creator<Package>() {
        @Override
        public Package createFromParcel(Parcel source) {
            return new Package(source);
        }

        @Override
        public Package[] newArray(int size) {
            return new Package[size];
        }
    };

}
