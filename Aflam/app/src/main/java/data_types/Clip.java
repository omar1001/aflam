package data_types;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by omar on 12/10/16.
 * Hello
 */
public class Clip implements Parcelable {
    public int id;
    public String clip;
    public Movie answer;
    public int answerMovieId;
    public List<Movie> choices;
    public int number;
    public boolean solved = false;

    public Clip() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.clip);
        dest.writeParcelable(this.answer, flags);
        dest.writeInt(this.answerMovieId);
        dest.writeTypedList(this.choices);
        dest.writeInt(this.number);
        dest.writeByte(this.solved ? (byte) 1 : (byte) 0);
    }

    protected Clip(Parcel in) {
        this.id = in.readInt();
        this.clip = in.readString();
        this.answer = in.readParcelable(Movie.class.getClassLoader());
        this.answerMovieId = in.readInt();
        this.choices = in.createTypedArrayList(Movie.CREATOR);
        this.number = in.readInt();
        this.solved = in.readByte() != 0;
    }

    public static final Creator<Clip> CREATOR = new Creator<Clip>() {
        @Override
        public Clip createFromParcel(Parcel source) {
            return new Clip(source);
        }

        @Override
        public Clip[] newArray(int size) {
            return new Clip[size];
        }
    };
}
