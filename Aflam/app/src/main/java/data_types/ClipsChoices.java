package data_types;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by omar on 12/10/16.
 * Hello
 */
public class ClipsChoices implements Parcelable {
    public int id;
    public int clipId;
    public int movieId;
    public int number;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeInt(this.clipId);
        dest.writeInt(this.movieId);
        dest.writeInt(this.number);
    }

    public ClipsChoices() {
    }

    protected ClipsChoices(Parcel in) {
        this.id = in.readInt();
        this.clipId = in.readInt();
        this.movieId = in.readInt();
        this.number = in.readInt();
    }

    public static final Parcelable.Creator<ClipsChoices> CREATOR = new Parcelable.Creator<ClipsChoices>() {
        @Override
        public ClipsChoices createFromParcel(Parcel source) {
            return new ClipsChoices(source);
        }

        @Override
        public ClipsChoices[] newArray(int size) {
            return new ClipsChoices[size];
        }
    };
}
