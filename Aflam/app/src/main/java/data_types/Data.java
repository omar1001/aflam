package data_types;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by omar on 12/18/16.
 * Hello
 */
public class Data implements Parcelable {
    public List<Clip> clips;
    public List<ClipsChoices> clipsChoices;
    public List<Movie> movies;
    public List<Package> packages;
    public List<PackagesClips> packagesClips;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.clips);
        dest.writeTypedList(this.clipsChoices);
        dest.writeTypedList(this.movies);
        dest.writeTypedList(this.packages);
        dest.writeTypedList(this.packagesClips);
    }

    public Data() {
    }

    protected Data(Parcel in) {
        this.clips = in.createTypedArrayList(Clip.CREATOR);
        this.clipsChoices = in.createTypedArrayList(ClipsChoices.CREATOR);
        this.movies = in.createTypedArrayList(Movie.CREATOR);
        this.packages = in.createTypedArrayList(Package.CREATOR);
        this.packagesClips = in.createTypedArrayList(PackagesClips.CREATOR);
    }

    public static final Parcelable.Creator<Data> CREATOR = new Parcelable.Creator<Data>() {
        @Override
        public Data createFromParcel(Parcel source) {
            return new Data(source);
        }

        @Override
        public Data[] newArray(int size) {
            return new Data[size];
        }
    };
}
