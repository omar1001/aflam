package util;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import data_types.Data;
import db.DBHandler;

import static util.HelperMethods.readFileFromAssets;

/**
 * Created by omar on 12/18/16.
 * Hello
 */
public class DataGetter {

    private Context mContext;
    private Fragment caller;
    private DBHandler dbHandler;
    private boolean preInstalledFetched = false;

    private static final String PREF_DATA_NAME = "NNN_DATA";
    private static final String CLIPS_NUMBER = "cn";
    private static final String CLIPS_CHOICES_NUMBER = "ccn";
    private static final String MOVIES_NUMBER = "mn";
    private static final String PACKAGES_NUMBER = "pn";
    private static final String PACKAGES_CLIPS_NUMBER = "pcn";

    private static final String BASE_URL = "http://aflam.quantum42.us:3000/check_data";

    public DataGetter(Fragment caller) {
        this.caller = caller;
        this.mContext = caller.getContext();
        this.dbHandler = new DBHandler(this.mContext);
    }

    public void loadData() {
        (new LocalGetter(caller)).execute();
    }

    public void loadOnlineData() {new OnlineGetter(caller).execute();}

    private class LocalGetter extends AsyncTask <Void, Void, Data>{
        private final String LOG_TAG = OnlineGetter.class.getSimpleName();
        Fragment caller;
        LocalGetter(Fragment caller) {
            this.caller = caller;
        }

        @Override
        protected Data doInBackground(Void[] params) {
            try {
                Data data = dbHandler.getData();

                SharedPreferences sharedPref = mContext
                        .getSharedPreferences(PREF_DATA_NAME, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();

                editor.putString(CLIPS_NUMBER
                        , Integer.toString(HelperMethods.getMaxNumberClip(data.clips)));
                editor.putString(CLIPS_CHOICES_NUMBER
                        , Integer.toString(HelperMethods.getMaxNumberClipsChoices(data.clipsChoices)));
                editor.putString(MOVIES_NUMBER
                        , Integer.toString(HelperMethods.getMaxNumberMovies(data.movies)));
                editor.putString(PACKAGES_NUMBER
                        , Integer.toString(HelperMethods.getMaxNumberPackages(data.packages)));
                editor.putString(PACKAGES_CLIPS_NUMBER
                        , Integer.toString(HelperMethods.getMaxNumberPackagesCLips(data.packagesClips)));

                editor.apply();
                return data;
            } catch (Exception e) {
                Log.e("-----------", " Exception in local getter" + e.toString());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Data data) {

            if(data.packages.size() == 0
                    && data.clips.size() == 0
                    && data.movies.size() == 0
                    && data.clipsChoices.size() == 0
                    && data.packagesClips.size() == 0
                    && !preInstalledFetched
                    ) {

                new PreInstalledGetter().execute();
                return;

            }

            ((PostFetch) caller).onPostFetch(data);

        }

    }

    private class OnlineGetter extends AsyncTask<Void, Void, Integer> {
        private final String LOG_TAG = OnlineGetter.class.getSimpleName();
        Fragment caller;
        OnlineGetter(Fragment caller) {
            this.caller = caller;
        }

        @Override
        protected Integer doInBackground(Void... params) {

            int packagesSize = 0;

            if(!HelperMethods.isNetworkAvailable(caller.getContext())) {
                return packagesSize;
            }

            // These two need to be declared outside the try/catch
            // so that they can be closed in the finally block.
            HttpURLConnection urlConnection = null;
            BufferedReader reader = null;

            // Will contain the raw JSON response as a string.
            String dataJSONResponse = null;

            try {

                SharedPreferences sharedPref = mContext
                        .getSharedPreferences(PREF_DATA_NAME, Context.MODE_PRIVATE);

                String clips_number = sharedPref.getString(CLIPS_NUMBER, "-1");
                String clips_choices_number = sharedPref.getString(CLIPS_CHOICES_NUMBER, "-1");
                String movies_number = sharedPref.getString(MOVIES_NUMBER, "-1");
                String packages_number = sharedPref.getString(PACKAGES_NUMBER, "-1");
                String packages_clips_number = sharedPref.getString(PACKAGES_CLIPS_NUMBER, "-1");


                final String PACKAGES_PARAM = "p";
                final String PACKAGES_CLIPS_PARAM = "pc";
                final String CLIPS_PARAM = "c";
                final String CLIPS_CHOICES_PARAM = "cc";
                final String MOVIES_PARAM = "m";

                Uri builtUri = Uri.parse(BASE_URL).buildUpon()
                        .appendQueryParameter(PACKAGES_PARAM, packages_number)
                        .appendQueryParameter(PACKAGES_CLIPS_PARAM, packages_clips_number)
                        .appendQueryParameter(CLIPS_PARAM, clips_number)
                        .appendQueryParameter(CLIPS_CHOICES_PARAM, clips_choices_number)
                        .appendQueryParameter(MOVIES_PARAM, movies_number)
                        .build();

                URL url = new URL(builtUri.toString());

                // Create the request to OpenWeatherMap, and open the connection
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();

                // Read the input stream into a String
                InputStream inputStream = urlConnection.getInputStream();
                StringBuffer buffer = new StringBuffer();
                if (inputStream == null) {
                    // Nothing to do.
                    return packagesSize;
                }
                reader = new BufferedReader(new InputStreamReader(inputStream));

                String line;
                while ((line = reader.readLine()) != null) {
                    // Since it's JSON, adding a newline isn't necessary (it won't affect parsing)
                    // But it does make debugging a *lot* easier if you print out the completed
                    // buffer for debugging.
                    buffer.append(line + "\n");
                }

                if (buffer.length() == 0) {
                    // Stream was empty.  No point in parsing.
                    return packagesSize;
                }
                dataJSONResponse = buffer.toString();

                /*
                Here we send the response json to database
                to save the data for a future local getter
                 */
                packagesSize = sendJsonToDatabase(dataJSONResponse);

            } catch (Exception e) {
                Log.e(LOG_TAG, "Error ", e);
                // If the code didn't successfully get the weather data, there's no point in attemping
                // to parse it.
                return packagesSize;
            } finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (final IOException e) {
                        Log.e("----err----" + LOG_TAG, "Error closing stream", e);
                    }
                }

            }
            return packagesSize;
        }

        @Override
        protected void onPostExecute(Integer packagesNumber) {
            ((PostFetch) caller).onPostOnline(packagesNumber);
        }

    }

    private int sendJsonToDatabase(String json) throws Exception {
        final String PACKAGES = "p";
        final String PACKAGES_CLIPS = "pc";
        final String CLIPS = "c";
        final String CLIPS_CHOICES = "cc";
        final String MOVIES = "m";
        int packagesSize = 0;
        JSONObject dataObject = new JSONObject(json);

        JSONArray packagesArray = dataObject.getJSONArray(PACKAGES);
        dbHandler.insertPackages(JsonHelpers.getPackagesFromJSONArray(packagesArray));

        packagesSize = packagesArray.length();

        JSONArray packagesCLipsArray = dataObject.getJSONArray(PACKAGES_CLIPS);
        dbHandler.insertPackagesClips(JsonHelpers.getPackagesClipsFromJSONArray(packagesCLipsArray));

        JSONArray clipsArray = dataObject.getJSONArray(CLIPS);
        dbHandler.insertClips(JsonHelpers.getClipsFromJSONArray(clipsArray));

        JSONArray clipsChoicesArray = dataObject.getJSONArray(CLIPS_CHOICES);
        dbHandler.insertClipsChoices(JsonHelpers.getClipsChoicesFromJSONArray(clipsChoicesArray));

        JSONArray moviesArray = dataObject.getJSONArray(MOVIES);
        dbHandler.insertMovies(JsonHelpers.getMoviesFromJSONArray(moviesArray));

        return packagesSize;
    }


    private class PreInstalledGetter extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            final String fileName = "json/pre_installed_data";
            try {
                String preInstalledJson = readFileFromAssets(mContext, fileName);
                sendJsonToDatabase(preInstalledJson);
            } catch (Exception e) {
                Log.e("----------", "pre getter error: " + e.toString());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            preInstalledFetched = true;
            new LocalGetter(caller).execute();
        }
    }

    public interface PostFetch {
        void onPostFetch(Data data);
        void onPostOnline(int packagesNumber);
    }



}
