package util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import data_types.Clip;
import data_types.ClipsChoices;
import data_types.Movie;
import data_types.Package;
import data_types.PackagesClips;

/**
 * Created by omar on 1/28/17.
 * Hello
 */
public class JsonHelpers {

    /*
    This class has static methods to extract data from JSON arrays
    extracts:
        Packages,
        Clips,
        Movies,
        Packages Clips,
        Clips Choices
    return extracted data in the form of List of the same data type.
     */

    private static final String ID = "i";
    private static final String POINTS = "p";
    private static final String CLIPS = "c";
    private static final String COLOR = "co";
    private static final String PACKAGE_ID = "pi";
    private static final String CLIP_ID = "ci";
    private static final String MOVIE_ID = "mi";
    private static final String MOVIE = "m";
    private static final String NUMBER = "n";
    private static final String CLIP = "c";
    private static final String ANSWER = "mi";


    static List<Package> getPackagesFromJSONArray(JSONArray array) throws Exception {
        List<Package> result = new ArrayList<>();

        for(int i=0; i<array.length(); i++) {
            Package pkg = new Package();
            JSONObject p = array.getJSONObject(i);
            pkg.id = p.getInt(ID);
            pkg.points = p.getInt(POINTS);
            pkg.clipsNumber = p.getInt(CLIPS);
            pkg.number = p.getInt(NUMBER);
            pkg.color = p.getString(COLOR);

            result.add(pkg);
        }

        return result;
    }

    static List<PackagesClips> getPackagesClipsFromJSONArray(JSONArray array) throws Exception {
        List<PackagesClips> result = new ArrayList<>();

        for(int i=0; i<array.length(); i++) {
            PackagesClips pkgsClips = new PackagesClips();
            JSONObject p = array.getJSONObject(i);
            pkgsClips.id = p.getInt(ID);
            pkgsClips.packageId = p.getInt(PACKAGE_ID);
            pkgsClips.clipId = p.getInt(CLIP_ID);
            pkgsClips.number = p.getInt(NUMBER);

            result.add(pkgsClips);
        }

        return result;
    }

    static List<Clip> getClipsFromJSONArray(JSONArray array) throws Exception {
        List<Clip> result = new ArrayList<>();

        for(int i=0; i<array.length(); i++) {
            Clip clip = new Clip();
            JSONObject p = array.getJSONObject(i);
            clip.id = p.getInt(ID);
            clip.answerMovieId = p.getInt(ANSWER);
            clip.clip = p.getString(CLIP);
            clip.number = p.getInt(NUMBER);

            result.add(clip);
        }

        return result;
    }

    static List<ClipsChoices> getClipsChoicesFromJSONArray(JSONArray array) throws Exception {
        List<ClipsChoices> result = new ArrayList<>();

        for(int i=0; i<array.length(); i++) {
            ClipsChoices clipsChoices = new ClipsChoices();
            JSONObject p = array.getJSONObject(i);
            clipsChoices.id = p.getInt(ID);
            clipsChoices.movieId = p.getInt(MOVIE_ID);
            clipsChoices.clipId = p.getInt(CLIP_ID);
            clipsChoices.number = p.getInt(NUMBER);

            result.add(clipsChoices);
        }

        return result;
    }

    static List<Movie> getMoviesFromJSONArray(JSONArray array) throws Exception {
        List<Movie> result = new ArrayList<>();

        for(int i=0; i<array.length(); i++) {
            Movie movie = new Movie();
            JSONObject p = array.getJSONObject(i);
            movie.id = p.getInt(ID);
            movie.name = p.getString(MOVIE);
            movie.number = p.getInt(NUMBER);

            result.add(movie);
        }

        return result;
    }
}
