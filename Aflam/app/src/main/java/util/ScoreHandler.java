package util;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import data_types.Clip;
import data_types.ClipsSolved;
import data_types.Data;
import data_types.Package;
import db.DBHandler;

/**
 * Created by omar on 1/6/17.
 * Hello
 */
public class ScoreHandler {
    private  static Callbacks mCaller;
    private Context mContext;
    private static final int PACKAGE_SCORE = 1;
    private static final int DECREMENT = 2;
    private static final int INCREMENT = 3;
    private static final int TOTAL_SCORE = 4;
    private static final int ADD_VIDEO_REWARD = 5;
    private static final int INITIAL_SCORE = 12;


    private static final int REWARD_VALUE = 10;

    public static List<ClipsSolved> solvedClips = new ArrayList<>();

    private static final String SCORE = "score";

    public ScoreHandler(Context context) {
        mContext = context;
    }

    public ScoreHandler( Context context, Callbacks caller) {
        mCaller = caller;
        mContext = context;
    }

    public static Data updatePackages(Data data) {
        for(int i=0; i<data.packages.size(); i++) {
            data.packages.set(i, HelperMethods.getUpdatedPackage(data.packages.get(i)));
        }
        return data;
    }

    public void savePackageScore(Package pkg) {
        new ScoreAsyncTask(PACKAGE_SCORE, pkg).execute();
    }

    public void decrementScore() {
        new ScoreAsyncTask(DECREMENT).execute();
    }

    public void saveVideoReward() {
        new ScoreAsyncTask(ADD_VIDEO_REWARD).execute();
    }

    public void getTotalScore() {
        new ScoreAsyncTask(TOTAL_SCORE).execute();
    }

    public void solveClip(Package pkg, Clip clip) {
        /*
        If you have decided to increment on solving each clip

            else: delete this method.
         */
        solvedClips.add(new ClipsSolved(pkg.id, clip.id));
        //new ScoreAsyncTask(INCREMENT).execute();
        new ClipSolvedAsync(clip).execute();
    }

    private class ScoreAsyncTask extends AsyncTask <Void, Void, Integer> {
        int task;
        Package pkg = null;
        ScoreAsyncTask(int task) {
            this.task = task;
        }
        ScoreAsyncTask(int task, Package pkg) {
            this(task);
            this.pkg = pkg;
        }

        @Override
        protected Integer doInBackground(Void... params) {
            int currentScore = 0;
            try {
                SharedPreferences sharedPref = mContext
                        .getSharedPreferences(SCORE, Context.MODE_PRIVATE);

                currentScore = sharedPref.getInt(SCORE, INITIAL_SCORE);

                switch (task) {
                    case PACKAGE_SCORE: {
                        currentScore += pkg.points;
                        break;
                    }
                    case DECREMENT: {
                        currentScore -= currentScore == 0? 0: 1;
                        break;
                    }
                    case INCREMENT: {
                        currentScore++;
                        break;
                    }
                    case ADD_VIDEO_REWARD: {
                        currentScore += REWARD_VALUE;
                        break;
                    }
                    default: break;
                }
                if(task != TOTAL_SCORE) {
                    SharedPreferences.Editor editor = sharedPref.edit();
                    applyScore(currentScore, editor);
                }
            } catch (Exception e) {
                Log.e("-----scorebackground--", e.toString());
            }
            return currentScore;
        }
        private void applyScore(int score, SharedPreferences.Editor editor) {
            editor.putInt(SCORE, score);
            editor.apply();
        }

        @Override
        protected void onPostExecute(Integer score) {
            mCaller.onFetchScore(score);
        }
    }

    private class ClipSolvedAsync extends AsyncTask <Void, Void, Void> {
        DBHandler dbHandler;
        Clip clip;
        ClipSolvedAsync(Clip clip) {
            this.clip = clip;
            dbHandler = new DBHandler(mContext);
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                dbHandler.updateClipSolved(clip);
            } catch (Exception e) {
                Log.e("------clp-slvd-bckgrnd-", e.toString());
            }
            return null;
        }
    }

    public interface Callbacks {
        void onFetchScore(int score);
    }

}
