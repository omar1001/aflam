package util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import data_types.Clip;
import data_types.ClipsChoices;
import data_types.Movie;
import data_types.Package;
import data_types.PackagesClips;

/**
 * Created by omar on 12/10/16.
 * Hello
 */
public class HelperMethods {
    /*
                        Utility static methods:
        *******************************************************
        * isNetworkAvailable
        * getMovieById
        * getChoicesByClipId
        * getClipById
        * getPackageClipsByPackageId
        *
        * getMaxNumberClip
        * getMaxNumberClipsChoices
        * getMaxNumberMovies
        * getMaxNumberPackages
        * getMaxNumberPackagesCLips
        *
        * getNextUnsolvedClip
        *
        * getUpdatedPackage
        *
        * updateSolvedClipsCount
        *
        * readFileFromAssets
        ********************************************************
     */

    private static int DEFAULT_NUMBER = -1;
    public static final String DATA = "data";
    public static final String PACKAGE_INDEX = "package_index";
    public static final String CLIP_INDEX = "clip_index";
    public static final String CLIPS = "clips";
    public static final String PACKAGE = "package";
    public static final String CLIP = "clip";

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }




    public static Movie getMovieById(int id, List<Movie> movies) {
        for(Movie movie: movies) {
            if(id == movie.id) return movie;
        }
        return null;
    }

    public static List<Movie> getChoicesByClipId(int clipId,
                                           List<ClipsChoices> choices,
                                           List<Movie> movies) {
        List<Movie> results = new ArrayList<>();
        for(ClipsChoices choice: choices) {
            if(clipId == choice.clipId) {
                results.add(getMovieById(choice.movieId, movies));
            }
        }
        return results;
    }

    private static Clip getClipById(int id, List<Clip> clips) {
        for(Clip clip: clips) {
            if(id == clip.id) {
                return clip;
            }
        }
        return null;
    }

    public static List<Clip> getPackageClipsByPackageId(
            int pkgId,
            List<Clip> clips,
            List<PackagesClips> packagesClips) {

        List<Clip> result = new ArrayList<>();
        for(PackagesClips pkgclips: packagesClips) {
            if(pkgId == pkgclips.packageId) {
                result.add(getClipById(pkgclips.clipId, clips));
            }
        }
        return result;
    }




    static int getMaxNumberClip(List<Clip> clips) {
        if(clips == null) return DEFAULT_NUMBER;
        if(clips.size() == 0) return DEFAULT_NUMBER;
        int max = DEFAULT_NUMBER;
        for(Clip clip: clips) {
            max = Math.max(max, clip.number);
        }
        return max;
    }

    static int getMaxNumberClipsChoices(List<ClipsChoices> clipsChoices) {
        if(clipsChoices == null) return DEFAULT_NUMBER;
        if(clipsChoices.size() == 0) return DEFAULT_NUMBER;
        int max = DEFAULT_NUMBER;
        for(ClipsChoices clipChoice: clipsChoices) {
            max = Math.max(max, clipChoice.number);
        }
        return max;
    }

    static int getMaxNumberMovies(List<Movie> Movies) {
        if(Movies == null) return DEFAULT_NUMBER;
        if(Movies.size() == 0) return DEFAULT_NUMBER;
        int max = DEFAULT_NUMBER;
        for(Movie movie: Movies) {
            max = Math.max(max, movie.number);
        }
        return max;
    }

    static int getMaxNumberPackages(List<Package> packages) {
        if(packages == null) return DEFAULT_NUMBER;
        if(packages.size() == 0) return DEFAULT_NUMBER;
        int max = DEFAULT_NUMBER;
        for(Package pkg: packages) {
            max = Math.max(max, pkg.number);
        }
        return max;
    }

    static int getMaxNumberPackagesCLips(List<PackagesClips> packagesClipses) {
        if(packagesClipses == null) return DEFAULT_NUMBER;
        if(packagesClipses.size() == 0) return DEFAULT_NUMBER;
        int max = DEFAULT_NUMBER;
        for(PackagesClips packageClips: packagesClipses) {
            max = Math.max(max, packageClips.number);
        }
        return max;
    }




    public static int getNextUnsolvedClip(Package pkg, int start) {
        int result = -1;
        for(int i=start + 1; i<pkg.clips.size(); i++) {
            Clip clip = pkg.clips.get(i);
            if(!clip.solved) {
                result = i;
                break;
            }
        }
        return result;
    }

    static Package getUpdatedPackage(Package pkg) {
        for(int i=0; i<pkg.clips.size(); i++) {
            for(int j=0; j<ScoreHandler.solvedClips.size(); j++) {
                if(ScoreHandler.solvedClips.get(j).clipID == pkg.clips.get(i).id) {
                    Clip clip = pkg.clips.get(i);
                    clip.solved = true;
                    pkg.clips.set(i, clip);
                    ScoreHandler.solvedClips.remove(j);
                    j--;
                }
            }
        }
        pkg = updateSolvedClipsCount(pkg);
        return pkg;
    }

    public static Package updateSolvedClipsCount(Package pkg) {
        int totalSolved = 0;
        for(int i=0; i<pkg.clips.size(); i++) {
            if(pkg.clips.get(i).solved) {
                totalSolved++;
            }
        }
        pkg.solvedClipsCount = totalSolved;
        return pkg;
    }




    static String readFileFromAssets(Context context, String fileName) {
        String str = null;
        try {

            InputStream inputStream = context.getAssets().open(fileName);

            int size = inputStream.available();

            byte[] buffer = new byte[size];

            inputStream.read(buffer);

            inputStream.close();

            str = new String(buffer, "UTF-8");

        } catch (Exception e) {
            Log.e("---------", "Reading from file error: " + e);
        }
        return str;
    }

    public static String convertDigitsToArabic(String text) {
        return text
                .replaceAll("0", "٠")
                .replaceAll("1", "١")
                .replaceAll("2", "٢")
                .replaceAll("3", "٣")
                .replaceAll("4", "٤")
                .replaceAll("5", "٥")
                .replaceAll("6", "٦")
                .replaceAll("7", "٧")
                .replaceAll("8", "٨")
                .replaceAll("9", "٩");
    }
}
