package db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import data_types.Clip;
import data_types.ClipsChoices;
import data_types.Data;
import data_types.Movie;
import data_types.Package;
import data_types.PackagesClips;
import util.HelperMethods;

/**
 * Created by omar on 12/10/16.
 * Hello
 */
public class DBHandler extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "af";
    private static int DATABASE_VERSION = 1;
    private static final String ID = "i";
    private static final String MOVIES_TABLE_NAME = "m";
    private static final String MOVIE_NAME = "mn";
    private static final String CLIPS_TABLE_NAME = "c";
    private static final String CLIP = "c";
    private static final String CLIP_ANSWER = "a";
    private static final String CLIPS_CHOICES_TABLE_NAME = "cc";
    private static final String CLIP_ID= "ci";
    private static final String MOVIE_ID= "mi";
    private static final String PACKAGES_TABLE_NAME= "p";
    private static final String POINTS = "p";
    private static final String CLIPS_NUMBER = "cn";
    private static final String COLOR = "c";
    private static final String PACKAGES_CLIPS_TABLE_NAME = "pc";
    private static final String PACKAGE_ID = "pi";
    private static final String NUMBER = "n";
    private static final String SOLVED = "sol";


    public DBHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String moviesCreateQuery = "CREATE TABLE " + MOVIES_TABLE_NAME + "( " +
                ID + " INT PRIMARY KEY, " +
                MOVIE_NAME + " TEXT, " +
                NUMBER + " INT" +
                ");";
        String clipsCreateQuery = "CREATE TABLE " + CLIPS_TABLE_NAME + "( " +
                ID + " INT PRIMARY KEY, " +
                CLIP + " TEXT, " +
                CLIP_ANSWER + " INT, " +
                NUMBER + " INT, " +
                SOLVED + " INT DEFAULT 0" +
                ");";
        String clipsChoicesCreateQuery = "CREATE TABLE " + CLIPS_CHOICES_TABLE_NAME + "( " +
                ID + " INT PRIMARY KEY, " +
                CLIP_ID + " INT, " +
                MOVIE_ID + " INT, " +
                NUMBER + " INT" +
                ");";
        String packagesCreate = "CREATE TABLE " + PACKAGES_TABLE_NAME + "( " +
                ID + " INT PRIMARY KEY, " +
                POINTS + " INT, " +
                CLIPS_NUMBER + " INT, " +
                COLOR + " TEXT, " +
                NUMBER + " INT" +
                ");";
        String packagesClipsCreateQuery = "CREATE TABLE " + PACKAGES_CLIPS_TABLE_NAME + "( " +
                ID + " INT PRIMARY KEY, " +
                PACKAGE_ID + " INT, " +
                CLIP_ID + " INT, " +
                NUMBER + " INT" +
                ");";

        db.execSQL(moviesCreateQuery);
        db.execSQL(clipsCreateQuery);
        db.execSQL(clipsChoicesCreateQuery);
        db.execSQL(packagesCreate);
        db.execSQL(packagesClipsCreateQuery);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) { }

    public Data getData() {
        Data data = new Data();
        data.movies = getMovies();
        data.clipsChoices = getClipsChoices();
        data.clips = getClips(data.movies, data.clipsChoices);
        data.packagesClips = getPackagesClips();
        data.packages = getPackages(
                data.clips,
                data.packagesClips
        );
        return data;
    }


    private List<Package> getPackages(
            List<Clip> clips,
            List<PackagesClips> packagesClips
    ) {

        //query packages
        String query = "SELECT * FROM " + PACKAGES_TABLE_NAME;

        SQLiteDatabase db =  getReadableDatabase();
        Cursor c = db.rawQuery(query, null);

        List<Package> packages= new ArrayList<>();

        c.moveToFirst();
        while (!c.isAfterLast()) {

            Package pkg = new Package();

            pkg.id = c.getInt(c.getColumnIndex(ID));
            pkg.points = c.getInt(c.getColumnIndex(POINTS));
            pkg.clipsNumber = c.getInt(c.getColumnIndex(CLIPS_NUMBER));
            pkg.color = c.getString(c.getColumnIndex(COLOR));
            pkg.number = c.getInt(c.getColumnIndex(NUMBER));
            pkg.clips = HelperMethods.getPackageClipsByPackageId(pkg.id, clips, packagesClips);

            pkg = HelperMethods.updateSolvedClipsCount(pkg);

            packages.add(pkg);
            c.moveToNext();
        }
        c.close();
        db.close();
        return packages;
    }

    private List<PackagesClips> getPackagesClips() {
        String query = "SELECT * FROM " + PACKAGES_CLIPS_TABLE_NAME;

        SQLiteDatabase db =  getReadableDatabase();
        Cursor c = db.rawQuery(query, null);

        List<PackagesClips> results = new ArrayList<>();

        c.moveToFirst();
        while (!c.isAfterLast()) {

            PackagesClips packageClip= new PackagesClips();
            packageClip.id = c.getInt(c.getColumnIndex(ID));
            packageClip.clipId = c.getInt(c.getColumnIndex(CLIP_ID));
            packageClip.packageId = c.getInt(c.getColumnIndex(PACKAGE_ID));
            packageClip.number = c.getInt(c.getColumnIndex(NUMBER));

            results.add(packageClip);
            c.moveToNext();
        }
        c.close();
        return results;
    }

    private List<ClipsChoices> getClipsChoices() {
        String query = "SELECT * FROM " + CLIPS_CHOICES_TABLE_NAME;

        SQLiteDatabase db =  getReadableDatabase();
        Cursor c = db.rawQuery(query, null);

        List<ClipsChoices> results = new ArrayList<>();

        c.moveToFirst();
        while (!c.isAfterLast()) {

            ClipsChoices clipChoice = new ClipsChoices();
            clipChoice.id = c.getInt(c.getColumnIndex(ID));
            clipChoice.clipId = c.getInt(c.getColumnIndex(CLIP_ID));
            clipChoice.movieId = c.getInt(c.getColumnIndex(MOVIE_ID));
            clipChoice.number = c.getInt(c.getColumnIndex(NUMBER));

            results.add(clipChoice);
            c.moveToNext();
        }
        c.close();

        return results;
    }

    private List<Clip> getClips(List<Movie> movies, List<ClipsChoices> choices) {
        String query = "SELECT * FROM " + CLIPS_TABLE_NAME + " ORDER BY " + ID + " DESC";

        SQLiteDatabase db =  getReadableDatabase();
        Cursor c = db.rawQuery(query, null);

        List<Clip> clips = new ArrayList<>();

        c.moveToFirst();
        while (!c.isAfterLast()) {
            Clip clip = new Clip();

            clip.id = c.getInt(c.getColumnIndex(ID));
            clip.clip = c.getString(c.getColumnIndex(CLIP));
            clip.number = c.getInt(c.getColumnIndex(NUMBER));
            clip.solved = c.getInt(c.getColumnIndex(SOLVED)) == 1;

            int answer = c.getInt(c.getColumnIndex(CLIP_ANSWER));
            clip.answer = HelperMethods.getMovieById(answer, movies);
            clip.answerMovieId = answer;
            clip.choices = HelperMethods.getChoicesByClipId(clip.id, choices, movies);

            clips.add(clip);
            c.moveToNext();
        }
        c.close();
        db.close();

        return clips;
    }

    private List<Movie> getMovies() {
        String query = "SELECT * FROM " + MOVIES_TABLE_NAME;

        SQLiteDatabase db =  getReadableDatabase();
        Cursor c = db.rawQuery(query, null);

        List<Movie> results = new ArrayList<>();

        c.moveToFirst();
        while (!c.isAfterLast()) {

            Movie movie = new Movie();

            movie.id = c.getInt(c.getColumnIndex(ID));
            movie.name = c.getString(c.getColumnIndex(MOVIE_NAME));
            movie.number = c.getInt(c.getColumnIndex(NUMBER));

            results.add(movie);
            c.moveToNext();
        }
        c.close();
        return results;
    }

    public void insertPackages(List<Package> packages) {
        SQLiteDatabase db = getWritableDatabase();
        for(Package pkg: packages) {
            ContentValues values = new ContentValues();
            values.put(ID, pkg.id);
            values.put(POINTS, pkg.points);
            values.put(COLOR, pkg.color);
            values.put(CLIPS_NUMBER, pkg.clipsNumber);
            values.put(NUMBER, pkg.number);

            db.insert(PACKAGES_TABLE_NAME, null, values);
        }
    }

    public void insertMovies(List<Movie> movies) {
        SQLiteDatabase db = getWritableDatabase();
        for(Movie movie: movies) {
            ContentValues values = new ContentValues();
            values.put(ID, movie.id);
            values.put(MOVIE_NAME, movie.name);
            values.put(NUMBER, movie.number);
            db.insert(MOVIES_TABLE_NAME, null, values);
        }
    }

    public void insertClips(List<Clip> clips) {
        SQLiteDatabase db = getWritableDatabase();
        for(Clip clip: clips) {
            ContentValues values = new ContentValues();
            values.put(ID, clip.id);
            values.put(CLIP, clip.clip);
            values.put(CLIP_ANSWER, clip.answerMovieId);
            values.put(NUMBER, clip.number);
            db.insert(CLIPS_TABLE_NAME, null, values);
        }
    }

    public void insertClipsChoices(List<ClipsChoices> clipsChoices) {
        SQLiteDatabase db = getWritableDatabase();
        for(ClipsChoices clipChoice: clipsChoices) {
            ContentValues values = new ContentValues();
            values.put(ID, clipChoice.id);
            values.put(CLIP_ID, clipChoice.clipId);
            values.put(MOVIE_ID, clipChoice.movieId);
            values.put(NUMBER, clipChoice.number);
            db.insert(CLIPS_CHOICES_TABLE_NAME, null, values);
        }
    }

    public void insertPackagesClips(List<PackagesClips> packagesClipss) {
        SQLiteDatabase db = getWritableDatabase();
        for(PackagesClips packagesClips: packagesClipss) {
            ContentValues values = new ContentValues();
            values.put(ID, packagesClips.id);
            values.put(PACKAGE_ID, packagesClips.packageId);
            values.put(CLIP_ID, packagesClips.clipId);
            values.put(NUMBER, packagesClips.number);
            db.insert(PACKAGES_CLIPS_TABLE_NAME, null, values);
        }
    }


    public void updateClipSolved(Clip clip) {
        SQLiteDatabase db = getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(SOLVED, 1);

        db.update(CLIPS_TABLE_NAME, values, ID +"="+clip.id, null);
    }
}
