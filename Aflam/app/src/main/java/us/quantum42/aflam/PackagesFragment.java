package us.quantum42.aflam;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.CycleInterpolator;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.unity3d.ads.IUnityAdsListener;
import com.unity3d.ads.UnityAds;

import java.util.ArrayList;
import java.util.List;

import adapters.PackagesAdapter;
import data_types.Data;
import data_types.Package;
import util.DataGetter;
import util.HelperMethods;
import util.ScoreHandler;

import static util.HelperMethods.convertDigitsToArabic;

/**
 * Created by omar on 12/18/16.
 * Hello
 */
public class PackagesFragment extends Fragment implements DataGetter.PostFetch{

    public Data data = null;
    final static String DATA = "DATA_OBJECT";
    PackagesAdapter packagesAdapter;
    View rootView;
    ProgressBar spinner;
    ProgressDialog dialog;
    private static final String Log_TAG = "PackagesFragment";
    public static final String DASHS = "---------";
    DataGetter dataGetter;
    List <Package> pkgs;
    ScoreHandler scoreHandler;
    TextView scoreText;
    SwipeRefreshLayout swipeRefreshLayout;
    int count = 0;
    private static int score = 3;

    public AlertDialog scoreDialog;
    private boolean unityAdShown = false;
    private ProgressDialog rewardedVideoAdLoadingDialog;
    public static boolean listItemClicked = false;


    private IUnityAdsListener iUnityAdsListener;
    private FirebaseAnalytics mFirebaseAnalytics;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_packages, container, false);

        ViewCompat.setLayoutDirection(rootView.findViewById(R.id.fragment_packages_top_banner), ViewCompat.LAYOUT_DIRECTION_RTL);

        spinner = (ProgressBar)rootView.findViewById(R.id.loading);
        spinner.setVisibility(View.VISIBLE);

        data = new Data();

        if (savedInstanceState == null) {
            getPackages();
        } else if (savedInstanceState.getParcelable(DATA) == null) {
            getPackages();
        } else {
            data = savedInstanceState.getParcelable(DATA);
        }

        pkgs = data.packages == null? new ArrayList<Package>(): data.packages;

        packagesAdapter = new PackagesAdapter(getActivity(), 0, pkgs);
        ListView packagesList = (ListView) rootView.findViewById(R.id.packages_list_view);
        packagesList.setAdapter(packagesAdapter);
        packagesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                if(listItemClicked) return;
                listItemClicked = true;

                // set your duration time
                int duration = 400;

                // ElasticAction : doAction
                //doAction(view, duration, 0.9f, 0.9f); // argument : View or ViewGroup, duration, scaleX, scaleY

                ViewCompat.animate(view).setDuration(duration).scaleX(0.5f).scaleY(0.5f)
                        .setInterpolator(new CycleInterpolator(0.5f))
                        .withEndAction(
                                new Runnable() {
                                    @Override
                                    public void run() {
                                        listItemClicked = false;
                                        if(position != 0) {
                                            Package prevPkg = pkgs.get(position - 1);
                                            if(prevPkg.solvedClipsCount == prevPkg.clipsNumber) {
                                                openClipsActivity(position);
                                            } else {
                                                Toast.makeText(getContext(),
                                                        getResources().getString(R.string.solve_prev),
                                                        Toast.LENGTH_SHORT).show();
                                            }
                                        } else {
                                            openClipsActivity(position);
                                        }
                                    }
                                }
                        ).withLayer()
                        .start();

            }
        });

        (rootView.findViewById(R.id.update_packages_button)).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getContext());
                        Bundle params = new Bundle();
                        params.putString("source", "update button");
                        mFirebaseAnalytics.logEvent("update_packages", params);
                        updatePackages();
                    }
                }
        );
        swipeRefreshLayout =
                (SwipeRefreshLayout)rootView.findViewById(R.id.packages_swipe_layout);

        swipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getContext());
                        Bundle params = new Bundle();
                        params.putString("source", "swipe");
                        mFirebaseAnalytics.logEvent("update_packages", params);

                        updatePackages();

                    }
                }
        );
        scoreText = (TextView)rootView.findViewById(R.id.score_fragment_packages);
        (rootView.findViewById(R.id.score_fragment_packages))
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getContext());
                        Bundle params = new Bundle();
                        params.putString("source", "click packages fragment");
                        mFirebaseAnalytics.logEvent("score_dialog", params);
                        openScoreDialog();
                    }
                });

        return rootView;
    }

    public void updatePackages() {
        if(!HelperMethods.isNetworkAvailable(getContext())) {
            Toast.makeText(
                    getContext()
                    , getResources().getString(R.string.no_internet_connection)
                    , Toast.LENGTH_LONG
            ).show();
            swipeRefreshLayout.setRefreshing(false);
            return;
        }
        //dialog = new ProgressDialog(getContext());
        //dialog.setMessage(getResources().getString(R.string.please_wait));
        //dialog.show();
        swipeRefreshLayout.setRefreshing(true);
        dataGetter.loadOnlineData();
    }

    private void openScoreDialog() {
        if(scoreDialog != null) {
            if (scoreDialog.isShowing()) {
                scoreDialog.dismiss();
            }
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_score, null);

        String scoreText = getResources().getString(R.string.score) + " " + HelperMethods.convertDigitsToArabic("" + score);
        ((TextView)dialogView.findViewById(R.id.score_text_score_dialog))
                .setText(scoreText);
        builder.setView(dialogView);
        scoreDialog = builder.create();
        (dialogView.findViewById(R.id.watch_ad_button_score_dialog)).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(score != 0) {
                            Toast.makeText(
                                    getContext(),
                                    getResources().getString(R.string.score_is_not_zero),
                                    Toast.LENGTH_SHORT
                            ).show();
                            return;
                        }
                        if(!HelperMethods.isNetworkAvailable(getContext())) {
                            Toast.makeText(
                                    getContext()
                                    , getResources().getString(R.string.no_internet_connection)
                                    , Toast.LENGTH_SHORT
                            ).show();

                            return;
                        }
                        rewardedVideoAdLoadingDialog = new ProgressDialog(getActivity());
                        rewardedVideoAdLoadingDialog.setMessage(getResources().getString(R.string.please_wait));
                        rewardedVideoAdLoadingDialog.show();
                        unityAdShown = false;
                        if(iUnityAdsListener == null) {
                            iUnityAdsListener = new IUnityAdsListener() {
                                @Override
                                public void onUnityAdsReady(String s) {
                                    if(unityAdShown) {
                                        return;
                                    }
                                    unityAdShown = true;
                                    rewardedVideoAdLoadingDialog.dismiss();
                                    UnityAds.show(getActivity());
                                }

                                @Override
                                public void onUnityAdsStart(String s) {

                                }

                                @Override
                                public void onUnityAdsFinish(String s, UnityAds.FinishState finishState) {
                                    unityAdShown = true;
                                    if(rewardedVideoAdLoadingDialog.isShowing()) rewardedVideoAdLoadingDialog.dismiss();
                                    if(finishState != UnityAds.FinishState.SKIPPED) {
                                        scoreHandler.saveVideoReward();
                                        Log.v("---------", "saving video reward");
                                        Toast.makeText(getActivity(), "تم إضافة النقاط", Toast.LENGTH_LONG).show();
                                        scoreDialog.dismiss();
                                    }
                                }

                                @Override
                                public void onUnityAdsError(UnityAds.UnityAdsError unityAdsError, String s) {
                                    Log.e("-------unity: ", s);
                                }
                            };
                        }
                        if(UnityAds.isInitialized()) {
                            UnityAds.setListener(iUnityAdsListener);
                            UnityAds.show(getActivity());
                        } else {
                            UnityAds.initialize(
                                    getActivity(),
                                    "1316147",
                                    iUnityAdsListener
                            );
                        }
                    }
                }
        );
        dialogView.findViewById(R.id.cancel_score_dialog)
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        scoreDialog.dismiss();
                    }
                });
        scoreDialog.show();

    }

    private void openClipsActivity(int packagesIndex) {
        Intent clipsActivityIntent = new Intent(getActivity(), ClipsActivity.class);
        clipsActivityIntent.putExtra(HelperMethods.DATA, data);
        clipsActivityIntent.putExtra(HelperMethods.PACKAGE_INDEX, packagesIndex);
        startActivity(clipsActivityIntent);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(DATA, data);
        super.onSaveInstanceState(outState);
    }


    @Override
    public void onStart() {
        super.onStart();
        try {
            if (ScoreHandler.solvedClips.size() != 0) {
                data = ScoreHandler.updatePackages(data);
                if (data != null) {
                    if (data.packages != null) {
                        packagesAdapter.clear();
                        packagesAdapter.addAll(data.packages);
                    }
                }
            }
        } catch (Exception e) {
            Log.e(Log_TAG, e.toString());
            getPackages();
        }

        scoreHandler = new ScoreHandler(getContext(), new ScoreHandler.Callbacks() {
            @Override
            public void onFetchScore(int mScore) {
                try {
                    score = mScore;
                    String scoreStr = getResources().getString(R.string.score) + " " +
                            HelperMethods.convertDigitsToArabic(Integer.toString(score));
                    scoreText.setText(scoreStr);
                } catch (Exception e) {
                    Log.e("-----onfetchscore", e.toString());
                }
            }
        });
        scoreHandler.getTotalScore();
    }

    @Override
    public void onResume() {
        super.onResume();
        if(pkgs.size() != 0) {
            if(pkgs.get(pkgs.size()-1).solvedClipsCount >= pkgs.get(pkgs.size()-1).clipsNumber) {
                mFirebaseAnalytics = FirebaseAnalytics.getInstance(getContext());
                Bundle params = new Bundle();
                params.putString("source", "on resume");
                mFirebaseAnalytics.logEvent("update_packages", params);
                updatePackages();
            }
        }
    }

    @Override
    public void onPostFetch(Data data) {
        if (data != null) {
            this.data = data;
            if(this.spinner != null) this.spinner.setVisibility(View.GONE);
            packagesAdapter.clear();
            packagesAdapter.addAll(data.packages);
        }
    }

    @Override
    public void onPostOnline(int packagesNumber) {
        //dialog.dismiss();
        try{
            swipeRefreshLayout.setRefreshing(false);
            getPackages();
            String message = packagesNumber == 0? getResources().getString(R.string.no_new_packages)
                    : convertDigitsToArabic(Integer.toString(packagesNumber)) +
                    getResources().getString(R.string.new_packages);
            Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
        } catch(Exception e) {
            Log.e("-----error", e.getMessage());
        }
    }

    private void getPackages() {
        dataGetter = dataGetter == null? new DataGetter(this): dataGetter;
        dataGetter.loadData();
    }

}
