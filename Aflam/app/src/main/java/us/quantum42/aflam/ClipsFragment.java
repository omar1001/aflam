package us.quantum42.aflam;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import adapters.ClipsAdapter;
import data_types.Clip;
import data_types.Package;
import util.HelperMethods;

/**
 * Created by omar on 12/18/16.
 * Hello
 */
public class ClipsFragment extends Fragment {
    View rootView;
    Package pkg;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_clips, container, false);
        if(savedInstanceState == null) {
            Bundle bundle = getArguments();
            pkg = bundle.getParcelable(HelperMethods.PACKAGE);
        } else {
            pkg = savedInstanceState.getParcelable(HelperMethods.PACKAGE);
        }

        List <Clip> clips = pkg == null? new ArrayList<Clip>(): pkg.clips;

        ClipsAdapter clipsAdapter = new ClipsAdapter(getActivity(), 0, clips);
        ListView clipsList = (ListView) rootView.findViewById(R.id.clips_list_view);
        clipsList.setAdapter(clipsAdapter);
        clipsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //TODO: open clip activity
                ((Callbacks) getActivity()).clipClickHandler(position);
            }
        });

        return rootView;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(HelperMethods.PACKAGE, pkg);
    }


    interface Callbacks {
        void clipClickHandler(int clip_number);
    }

}
