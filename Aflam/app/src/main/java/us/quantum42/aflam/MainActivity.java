package us.quantum42.aflam;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
/*
        TextView play = (TextView) findViewById(R.id.us.aflam);
        Typeface balloonFont = Typeface.createFromAsset(getAssets(),  "fonts/try2" +
                ".otf");
        play.setTypeface(balloonFont);
        */

    }


    public void openPlayActivity(View view) {
        startActivity(new Intent(this, PackagesActivity.class));
    }
}
