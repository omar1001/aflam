package us.quantum42.aflam;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.InterstitialAd;
import com.facebook.ads.InterstitialAdListener;

import static util.HelperMethods.isNetworkAvailable;

public class PackagesActivity extends AppCompatActivity {

    PackagesFragment packagesFragment;
    private static final String PACKAGES_FRAGMENT_TAG = "PKSFT";

    // Audience network variables.
    private InterstitialAd interstitialAd;
    private static int interstitialApproachCount = 0;
    private static final int interstitialApproachEnd = 5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pakages);

        if (savedInstanceState == null) {
            packagesFragment = new PackagesFragment();

            getSupportFragmentManager().beginTransaction()
                    .add(R.id.packages_activity_container, packagesFragment, PACKAGES_FRAGMENT_TAG)
                    .commit();
        }

    }


    @Override
    protected void onResume() {
        super.onResume();
        approachInterstitial();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (interstitialAd != null) {
            interstitialAd.destroy();
        }
    }

    //this method check for the times of the request of the interstitial
    // if it's the first time to check which will the first time the user
    // opens the app, or it will be the end of the interval of the requests.
    // > load the ad when needed not before, then show immediately
    private void approachInterstitial() {
        if(!isNetworkAvailable(this)) {
            return;
        }
        if(interstitialApproachCount == 0) {
            loadInterstitialAd();
        }
        interstitialApproachCount =
                interstitialApproachCount == interstitialApproachEnd
                        ? 0: interstitialApproachCount + 1;
    }

    private void loadInterstitialAd() {
        interstitialAd = new InterstitialAd(this, "153161271860590_187139951796055");
        interstitialAd.setAdListener(
                new InterstitialAdListener() {
                    @Override
                    public void onInterstitialDisplayed(Ad ad) {

                    }

                    @Override
                    public void onInterstitialDismissed(Ad ad) {

                    }

                    @Override
                    public void onError(Ad ad, AdError adError) {

                    }

                    @Override
                    public void onAdLoaded(Ad ad) {
                        interstitialAd.show();
                    }

                    @Override
                    public void onAdClicked(Ad ad) {

                    }
                }
        );
        interstitialAd.loadAd();
    }

}
