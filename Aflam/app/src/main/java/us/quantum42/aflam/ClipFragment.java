package us.quantum42.aflam;

import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import adapters.ChoicesAdapter;
import data_types.Clip;
import data_types.Movie;
import util.HelperMethods;
import util.Swipe;


public class ClipFragment extends Fragment {

    Clip clip;
    String pkgColor;
    private LinearLayout baseLayout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_clip, container, false);
        if(savedInstanceState == null) {
            Bundle bundle = getArguments();
            clip = bundle.getParcelable(HelperMethods.CLIP);
            pkgColor = bundle.getString(ClipsActivity.PACKAGE_COLOR);
        } else {
            clip = savedInstanceState.getParcelable(HelperMethods.CLIP);
            pkgColor = savedInstanceState.getString(ClipsActivity.PACKAGE_COLOR);
        }

        ((GradientDrawable)rootView.findViewById(R.id.clip_background_clip)
                .getBackground()).setColor(Color.parseColor('#' + pkgColor));

        TextView clipText = ((TextView) rootView.findViewById(R.id.clip));

        Typeface balloonFont = Typeface.createFromAsset(getActivity().getAssets(),  "fonts/clip" +
                ".ttf");
        clipText.setTypeface(balloonFont, Typeface.BOLD);
        clipText.setText(clip.clip);

        List<Movie> choices = clip == null? new ArrayList<Movie>(): clip.choices;
        Collections.shuffle(choices);
        ChoicesAdapter choicesAdapter = clip.solved?
                new ChoicesAdapter(getActivity(), 0, choices, clip.answerMovieId):
                new ChoicesAdapter(getActivity(), 0, choices);

        ListView choicesList= (ListView) rootView.findViewById(R.id.choices_grid_view);
        choicesList.setAdapter(choicesAdapter);
        choicesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if(!clip.solved) {
                    /*
                    String answer = clip.choices.get(position).id == clip.answerMovieId?
                            "Your answer is right": "Your answer is wrong";
                    Log.e("------choiceClick", answer + ", answerMovieId: " + clip.answerMovieId +
                            ",\nid from choices list: " + clip.choices.get(position).id +
                            ",\nmovie name from choices list: " + clip.choices.get(position).name +
                            ",\nmovie name from clip: " + clip.answer.name
                    );
*/
                    if(clip.choices.get(position).id == clip.answerMovieId) {
                        //TODO: SHOW THE USER THAT THE ANSWER IS RIGHT
                        ((TextView)view.findViewById(R.id.clip_list_item_text)).setTextColor(Color.parseColor("green"));
                        ((Callbacks)getActivity()).rightChoiceHandler();
                    } else {
                        ((TextView)view.findViewById(R.id.clip_list_item_text)).setTextColor(Color.parseColor("red"));
                        ((Callbacks)getActivity()).wrongChoiceHandler();
                    }
                }

            }
        });
        baseLayout = (LinearLayout)rootView.findViewById(R.id.clip_background_clip);
        swipeListen();
//        (rootView.findViewById(R.id.clip_list_item)).setOnClickListener(
//                new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        ((Callbacks) getActivity()).openClipsList();
//                    }
//                }
//        );


        return rootView;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(HelperMethods.CLIP, clip);
        outState.putString(ClipsActivity.PACKAGE_COLOR, pkgColor);
    }
    private void swipeListen() {
        Swipe.detect(baseLayout, new Swipe.SwipeCallback() {
            @Override
            public void onSwipeTop() {

            }

            @Override
            public void onSwipeRight() {
                ((Callbacks)getActivity()).nextClip();
            }

            @Override
            public void onSwipeBottom() {

            }

            @Override
            public void onSwipeLeft() {
                ((Callbacks)getActivity()).prevClip();
            }
        });
    }


    interface Callbacks {
        void openClipsList();
        void rightChoiceHandler();
        void wrongChoiceHandler();
        void prevClip();
        void nextClip();
    }
}
