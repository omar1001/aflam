package us.quantum42.aflam;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.codemybrainsout.ratingdialog.RatingDialog;
import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.AdListener;
import com.facebook.ads.AdSize;
import com.facebook.ads.AdView;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.unity3d.ads.IUnityAdsListener;
import com.unity3d.ads.UnityAds;

import data_types.Data;
import data_types.Package;
import util.HelperMethods;
import util.ScoreHandler;

import static util.HelperMethods.convertDigitsToArabic;

public class ClipsActivity extends AppCompatActivity implements ClipsFragment.Callbacks,
ClipFragment.Callbacks {

    /*
    This activity controls the playing
        it opens the first unsolved clip in a clipfragment
        if all clips are solved it opens a list of the clips in a clipsfragment
        also in clip fragment there will be a button to show list
            of clips by calling callback method implemented here.
     */

    Data data;
    Package pkg;
    int packageIndex = -1;
    int currentClip = -1;
    ScoreHandler scoreHandler;
    public static final String PACKAGE_COLOR = "package_color";

    private static final String CLIPS_LIST_FRAGMENT = "1";
    private static final String CLIP_FRAGMENT = "2";
    private static final int RIGHT = 1;
    private static final int LEFT = 0;
    private boolean unityAdShown = false;

    private static int score;

    private AdView adView;
    public static AlertDialog scoreDialog;

    private ProgressDialog rewardedVideoAdLoadingDialog;

    private AppCompatActivity appCompatActivity = this;
    public AlertDialog.Builder builder;
    private IUnityAdsListener iUnityAdsListener = null;
    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clips);

        if(savedInstanceState == null) {
            Intent intent = getIntent();
            data = intent.getParcelableExtra(HelperMethods.DATA);
            packageIndex = intent.getIntExtra(HelperMethods.PACKAGE_INDEX, 0);
            pkg = data.packages.get(packageIndex);
        } else if(savedInstanceState.containsKey(HelperMethods.DATA) &&
                savedInstanceState.containsKey(HelperMethods.PACKAGE_INDEX)) {
            data = savedInstanceState.getParcelable(HelperMethods.DATA);
            packageIndex = savedInstanceState.getInt(HelperMethods.PACKAGE_INDEX);
            pkg = data.packages.get(packageIndex);
            return;
        }

        (findViewById(R.id.clips_activity_back_button)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFirebaseAnalytics = FirebaseAnalytics.getInstance(getBaseContext());
                mFirebaseAnalytics.logEvent("clips_list_click", null);

                onBackPressed();
            }
        });
        (findViewById(R.id.clips_activity_back_button_image)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFirebaseAnalytics = FirebaseAnalytics.getInstance(getBaseContext());
                mFirebaseAnalytics.logEvent("clips_list_click", null);
                onBackPressed();
            }
        });
        (findViewById(R.id.score_clip_activity)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFirebaseAnalytics = FirebaseAnalytics.getInstance(getBaseContext());
                Bundle params = new Bundle();
                params.putString("source", "click clips activity");
                mFirebaseAnalytics.logEvent("score_dialog", params);
                openScoreDialog();
            }
        });

        startFragment();
        scoreHandler = initializeScoreHandler();
        scoreHandler.getTotalScore();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(HelperMethods.DATA, data);
        outState.putInt(HelperMethods.PACKAGE_INDEX, packageIndex);
    }
    @Override
    protected void onDestroy() {
        if (adView != null) {
            adView.destroy();
        }
        super.onDestroy();
    }

    @Override
    protected void onStart() {
        super.onStart();
        loadBannerAd();
    }

    @Override
    public void onBackPressed() {
        try {
            Fragment currentFragment = getSupportFragmentManager().findFragmentByTag(CLIP_FRAGMENT);
            if (currentFragment != null && currentFragment.isVisible()) {
                openClipsListFragment();
            } else {
                super.onBackPressed();
            }
        } catch (Exception e) {
            super.onBackPressed();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void startFragment() {
        /*
        Here we check if the package has all clips solved
            then: we show the list of all clips
            else: we show the first unsolved clip
         */
        currentClip = HelperMethods.getNextUnsolvedClip(pkg, currentClip);
        if(currentClip == -1) {
            //open list of all clips.
            openClipsListFragment();
        } else {
            openClipFragment(currentClip);
        }
    }

    private void openClipsListFragment() {
        ClipsFragment clipsFragment = new ClipsFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(HelperMethods.PACKAGE, data.packages.get(packageIndex));
        clipsFragment.setArguments(bundle);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.clips_activity_container, clipsFragment, CLIPS_LIST_FRAGMENT)
                .commit();
    }
    private void openClipsListFragment(int animationDirection) {
        ClipsFragment clipsFragment = new ClipsFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(HelperMethods.PACKAGE, data.packages.get(packageIndex));
        clipsFragment.setArguments(bundle);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if(animationDirection == LEFT)
            transaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left);
        else
            transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
        transaction.replace(R.id.clips_activity_container, clipsFragment, CLIPS_LIST_FRAGMENT)
                .commit();
    }

    private void openClipFragment(int index) {
        ClipFragment clipFragment = new ClipFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(HelperMethods.CLIP, pkg.clips.get(index));
        bundle.putString(PACKAGE_COLOR, pkg.color);
        clipFragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.clips_activity_container, clipFragment, CLIP_FRAGMENT)
                .commit();

    }
    private void openClipFragment(int index, int animationDirection) {
        ClipFragment clipFragment = new ClipFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(HelperMethods.CLIP, pkg.clips.get(index));
        bundle.putString(PACKAGE_COLOR, pkg.color);
        clipFragment.setArguments(bundle);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if(animationDirection == LEFT)
            transaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left);
        else
            transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
        transaction.replace(R.id.clips_activity_container, clipFragment, CLIP_FRAGMENT)
                .commit();

    }
    private void openScoreDialog() {
        if(scoreDialog != null) {
            if(scoreDialog.isShowing()) {
                scoreDialog.dismiss();
            }
        }
        builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_score, null);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            dialogView.setBackground(getDrawable(R.drawable.dialog_background_card));
        }
        String scoreText = getResources().getString(R.string.score) + " " + HelperMethods.convertDigitsToArabic("" + score);
        ((TextView)dialogView.findViewById(R.id.score_text_score_dialog))
                .setText(scoreText);
        builder.setView(dialogView);

        scoreDialog = builder.create();
        (dialogView.findViewById(R.id.watch_ad_button_score_dialog)).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(score != 0) {
                            Toast.makeText(
                                    appCompatActivity,
                                    getResources().getString(R.string.score_is_not_zero),
                                    Toast.LENGTH_SHORT
                            ).show();
                            return;
                        }
                        if(!HelperMethods.isNetworkAvailable(appCompatActivity)) {
                            Toast.makeText(
                                    appCompatActivity
                                    , getResources().getString(R.string.no_internet_connection)
                                    , Toast.LENGTH_SHORT
                            ).show();

                            return;
                        }
                        rewardedVideoAdLoadingDialog = new ProgressDialog(appCompatActivity);
                        rewardedVideoAdLoadingDialog.setMessage(getResources().getString(R.string.please_wait));
                        rewardedVideoAdLoadingDialog.show();
                        unityAdShown = false;
                        if(iUnityAdsListener == null) {
                            iUnityAdsListener = new IUnityAdsListener() {
                                @Override
                                public void onUnityAdsReady(String s) {
                                    if(unityAdShown) {
                                        return;
                                    }
                                    unityAdShown = true;
                                    rewardedVideoAdLoadingDialog.dismiss();
                                    UnityAds.show(appCompatActivity);
                                }

                                @Override
                                public void onUnityAdsStart(String s) {

                                }

                                @Override
                                public void onUnityAdsFinish(String s, UnityAds.FinishState finishState) {
                                    unityAdShown = true;
                                    if(rewardedVideoAdLoadingDialog.isShowing()) rewardedVideoAdLoadingDialog.dismiss();
                                    if(finishState != UnityAds.FinishState.SKIPPED) {
                                        scoreHandler.saveVideoReward();
                                        Toast.makeText(appCompatActivity, "تم إضافة النقاط", Toast.LENGTH_LONG).show();
                                        scoreDialog.dismiss();
                                    }
                                }

                                @Override
                                public void onUnityAdsError(UnityAds.UnityAdsError unityAdsError, String s) {
                                    Log.e("-------unity: ", s);
                                }
                            };
                        }
                        if(UnityAds.isInitialized()) {
                            UnityAds.setListener(iUnityAdsListener);
                            UnityAds.show(appCompatActivity);
                        } else {
                            UnityAds.initialize(
                                    appCompatActivity,
                                    "1316147",
                                    iUnityAdsListener
                            );
                        }
                    }
                }
        );

        dialogView.findViewById(R.id.cancel_score_dialog)
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        scoreDialog.dismiss();
                    }
                });

        scoreDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                try {
                    if(score < 1) {
                        finish();
                    }
                } catch(Exception e) {
                    Log.e("---------", "Score dialog onDismiss error: " + e);
                }
            }
        });
        scoreDialog.show();
    }

    @Override
    public void clipClickHandler(int clip_number) {
        currentClip = clip_number;
        openClipFragment(clip_number);
    }

    @Override
    public void openClipsList() {
        openClipsListFragment();
    }

    @Override
    public void rightChoiceHandler() {
        scoreHandler.solveClip(pkg, pkg.clips.get(currentClip));
        pkg.clips.get(currentClip).solved = true;
        currentClip = HelperMethods.getNextUnsolvedClip(pkg, currentClip);
        scoreHandler = initializeScoreHandler();
        showRating();

        if(currentClip != -1) {
            openClipFragment(currentClip, LEFT);
        } else {
            if(HelperMethods.getNextUnsolvedClip(pkg, -1) == -1) {
                scoreHandler.savePackageScore(pkg);
                //TODO: Handle package score end.
                finish();
            } else {
                openClipsListFragment(LEFT);
            }
        }
    }

    private void showRating() {
        final RatingDialog ratingDialog = new RatingDialog.Builder(this)
                .threshold(3)
                .session(15)
                .positiveButtonText("Not now")
                .onRatingBarFormSumbit(new RatingDialog.Builder.RatingDialogFormListener(){
                    @Override
                    public void onFormSubmitted(String feedback) {
                        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getApplicationContext());
                        Bundle params = new Bundle();
                        params.putString("feedback", feedback);
                        mFirebaseAnalytics.logEvent("NegativeRate", params);
                    }
                }).build();

        ratingDialog.show();
    }

    @Override
    public void wrongChoiceHandler() {
        scoreHandler = initializeScoreHandler();
        scoreHandler.decrementScore();
        showRating();
    }

    private void loadBannerAd() {
        RelativeLayout adViewContainer = (RelativeLayout) findViewById(R.id.adViewContainer);
        adView = new AdView(this, "153161271860590_153178781858839", AdSize.BANNER_320_50);
        adViewContainer.addView(adView);
        adView.setAdListener(new AdListener() {
            @Override
            public void onError(Ad ad, AdError adError) {
                Log.v("--------banner", adError.getErrorMessage());
            }

            @Override
            public void onAdLoaded(Ad ad) {
            }

            @Override
            public void onAdClicked(Ad ad) {

            }
        });
        adView.loadAd();
    }

    @Override
    public void prevClip() {
        if(currentClip - 1 < 0) openClipsListFragment(RIGHT);
        else openClipFragment(--currentClip, RIGHT);
    }

    @Override
    public void nextClip() {
        if(currentClip + 1 >= pkg.clipsNumber) openClipsListFragment(LEFT);
        else openClipFragment(++currentClip, LEFT);
    }

    private ScoreHandler initializeScoreHandler() {
        return scoreHandler != null? scoreHandler: new ScoreHandler(this, new ScoreHandler.Callbacks() {
            @Override
            public void onFetchScore(int mScore) {
                try {
                    score = mScore;
                    String scoreText = getResources().getString(R.string.score) + " "
                            + convertDigitsToArabic(Integer.toString(score));
                    ((TextView)findViewById(R.id.score_clip_activity)).setText(scoreText);
                    if(score < 1) {
                        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getApplicationContext());
                        Bundle params = new Bundle();
                        params.putString("source", "score is zero");
                        mFirebaseAnalytics.logEvent("score_dialog", params);
                        openScoreDialog();
                    }
                } catch (Exception e) {
                    Log.e("-----onPostScoreAct-", e.toString());
                }
            }
        });
    }

}
