package adapters;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import data_types.Clip;
import us.quantum42.aflam.R;

/**
 * Created by omar on 1/5/17.
 * Hello
 */
public class ClipsAdapter extends ArrayAdapter<Clip> {
    private Context context;
    private List<Clip> clips;
    public ClipsAdapter(Context context, int resource, List<Clip> objects) {
        super(context, resource, objects);
        this.context = context;
        this.clips = objects;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        View row = convertView;
        ClipHolder holder;

        if(row == null) {
            holder = new ClipHolder();
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(R.layout.clips_list_item, parent, false);

            holder.clipNumber = (TextView) row.findViewById(R.id.clip_number_clips_list);
            holder.solved = (ImageView) row.findViewById(R.id.clips_solved);

            row.setTag(holder);
        } else {
            holder = (ClipHolder) row.getTag();
        }
        Clip clip = clips.get(position);

        String clipNumberTitle = clip.clip; //" " + context.getResources().getString(R.string.clip)
                //+ " " +  convertDigitsToArabic(Integer.toString(position + 1));

        if(clip.solved) {
            holder.solved.setVisibility(View.VISIBLE);
        } else {
            holder.solved.setVisibility(View.GONE);
        }

        holder.clipNumber.setText(clipNumberTitle);

        return row;
    }

    private static class ClipHolder {
        TextView clipNumber;
        ImageView solved;
    }
}