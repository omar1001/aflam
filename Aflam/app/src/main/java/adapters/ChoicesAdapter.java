package adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import data_types.Movie;
import us.quantum42.aflam.R;

/**
 * Created by omar on 1/5/17.
 * Hello
 */
public class ChoicesAdapter extends ArrayAdapter<Movie> {

    private Context context;
    private List<Movie> choices;
    private boolean solved = false;
    private int answerId = -1;
    public ChoicesAdapter(Context context, int resource, List<Movie> objects) {
        super(context, resource, objects);
        this.context = context;
        this.choices = objects;
    }
    public ChoicesAdapter(Context context, int resource, List<Movie> objects, int answerId) {
        this(context, resource, objects);
        this.solved = true;
        this.answerId = answerId;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        View row = convertView;
        ChoiceHolder holder;

        if(row == null)
        {
            holder = new ChoiceHolder();
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(R.layout.clip_list_item, parent, false);

            holder.choice = (TextView) row.findViewById(R.id.clip_list_item_text);

            Typeface balloonFont = Typeface.createFromAsset(context.getAssets(),  "fonts/clip" +
                    ".ttf");
            holder.choice.setTypeface(balloonFont);
            row.setTag(holder);
        }
        else
        {
            holder = (ChoiceHolder) row.getTag();
        }
        Movie choice = choices.get(position);

        String choiceTitle = choice.name;

        holder.choice.setText(choiceTitle);
        holder.choice.setTextColor(Color.BLACK);
        if(solved) {
            if(answerId == choice.id) {
                holder.choice.setTextColor(Color.parseColor("green"));
            }
        }

        return row;
    }

    private static class ChoiceHolder {
        TextView choice;
    }
}
