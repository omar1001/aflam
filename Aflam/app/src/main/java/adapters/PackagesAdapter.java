package adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import data_types.Package;
import us.quantum42.aflam.R;

import static util.HelperMethods.convertDigitsToArabic;

/**
 * Created by omar on 1/4/17.
 * Hello
 */
public class PackagesAdapter extends ArrayAdapter<Package> {
    private Context context;
    private List<Package> pkgs;
    public PackagesAdapter(Context context, int resource, List<Package> objects) {
        super(context, resource, objects);
        this.context = context;
        this.pkgs = objects;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        View row = convertView;
        PackageHolder holder = null;

        if(row == null)
        {
            holder = new PackageHolder();
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(R.layout.packages_list_item, parent, false);

            holder.packageTitle = (TextView) row.findViewById(R.id.package_title_list_item);
            holder.quotesNumber = (TextView) row.findViewById(R.id.quotes_list_item);
            holder.pointsNumber = (TextView) row.findViewById(R.id.points_list_item);
            holder.solvedClipsNumber = (TextView) row.findViewById(R.id.solved_clips_package_list);
            holder.lock = (ImageView) row.findViewById(R.id.packages_lock);

            row.setTag(holder);
        }
        else
        {
            holder = (PackageHolder) row.getTag();
        }
        Package pkg = pkgs.get(position);

        String packageTitle = context.getResources().getString(R.string.pkg)
                + convertDigitsToArabic(Integer.toString(position + 1));
        String quotesTitle = context.getResources().getString(R.string.clips)
                + convertDigitsToArabic(Integer.toString(pkg.clipsNumber));
        String pointsTitle = context.getResources().getString(R.string.package_score)
                + convertDigitsToArabic(Integer.toString(pkg.points));
        String solvedClipsTitle = convertDigitsToArabic(Integer.toString(pkg.solvedClipsCount))
                + "\\" +
                convertDigitsToArabic(Integer.toString(pkg.clipsNumber));

        holder.packageTitle.setText(packageTitle);
        holder.quotesNumber.setText(quotesTitle);
        holder.pointsNumber.setText(pointsTitle);
        holder.solvedClipsNumber.setText(solvedClipsTitle);

        if(position != 0) {
            Package prevPkg = pkgs.get(position - 1);
            if(prevPkg.solvedClipsCount != prevPkg.clipsNumber) {
                holder.solvedClipsNumber.setVisibility(View.GONE);
                holder.lock.setVisibility(View.VISIBLE);
            } else {
                holder.solvedClipsNumber.setVisibility(View.VISIBLE);
                holder.lock.setVisibility(View.GONE);
            }
        } else {
            holder.solvedClipsNumber.setVisibility(View.VISIBLE);
            holder.lock.setVisibility(View.GONE);
        }

        ((GradientDrawable)row.findViewById(R.id.relative_layout_list_item)
                .getBackground()).setColor(Color.parseColor('#' + pkg.color));

        return row;
    }

    private static class PackageHolder {
        TextView packageTitle;
        TextView quotesNumber;
        TextView pointsNumber;
        TextView solvedClipsNumber;
        ImageView lock;
    }
}
