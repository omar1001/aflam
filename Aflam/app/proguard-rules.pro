# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /home/omar/Android/Sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}
#-keep class com.google.android.gms.* { *; }
#-keep class com.google.android.gms.* { public *; }
#-keep class com.google.android.*
#-dontwarn com.google.**
#-keep class com.google.android.gms.internal.**
-keep class org.apache.http.** { *; }
-keep class com.google.android.gms.**

-dontwarn com.google.android.gms.**
-dontwarn com.google.ads.**
-dontwarn org.apache.http.**
-dontwarn android.net.**

-dontnote com.google.android.gms.**
-dontnote org.apache.http.**
-dontnote com.android.net.http.**
-dontnote android.net.**